package com.example.craigblackburn.cs3200_assignment_3;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.widget.ImageView;

/**
 * Created by craigblackburn on 10/1/16.
 */

public class CameraFeedThread extends Thread {

    private final ImageView imageView;
    private final Drawable[] images;
    private final Integer waitTime;
    private MainActivity activity;
    public Boolean didPause = false;
    private Integer index = 0;
    private Boolean keepRunning = false;

    public CameraFeedThread(@NonNull MainActivity activity, ImageView imageView, Drawable[] images, Integer waitTime) {
        this.imageView = imageView;
        this.images = images;
        this.waitTime = waitTime;
        this.activity = activity;
    }



    public void run() {
        if (keepRunning) {
            try {
                sleep(waitTime);

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (index > images.length - 1) {
                            index = 0;
                        }
                        imageView.setImageDrawable(images[index++]);
                    }
                });

                run();
            } catch (InterruptedException e) {
                currentThread().interrupt();
            }
        }

    }


    public void startFeed() {
        didPause = false;
        keepRunning = true;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (index > images.length - 1) {
                    index = 0;
                }

                imageView.setImageDrawable(images[index++]);
                start();
            }
        });

    }

    public void stopFeed() {
        keepRunning = false;
        interrupt();
    }

    public void pause() {
        didPause = true;
        stopFeed();
    }


}
