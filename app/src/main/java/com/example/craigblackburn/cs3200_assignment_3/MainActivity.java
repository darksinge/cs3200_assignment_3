package com.example.craigblackburn.cs3200_assignment_3;

import android.app.Activity;
import android.graphics.Camera;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity  {

    private CameraFeedThread thread_01;
    private CameraFeedThread thread_02;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Drawable[] imageSet_01 = {
                ContextCompat.getDrawable(this, R.drawable.gif1_1),
                ContextCompat.getDrawable(this, R.drawable.gif1_2),
                ContextCompat.getDrawable(this, R.drawable.gif1_3),
                ContextCompat.getDrawable(this, R.drawable.gif1_4),
                ContextCompat.getDrawable(this, R.drawable.gif1_5),
        };

        Drawable[] imageSet_02 = {
                ContextCompat.getDrawable(this, R.drawable.gif2_1),
                ContextCompat.getDrawable(this, R.drawable.gif2_2),
                ContextCompat.getDrawable(this, R.drawable.gif2_3),
                ContextCompat.getDrawable(this, R.drawable.gif2_4),
                ContextCompat.getDrawable(this, R.drawable.gif2_5),
        };

        final ImageView imageView01 = (ImageView) findViewById(R.id.imageView_01);
        thread_01 = new CameraFeedThread(this, imageView01, imageSet_01, 2000);

        final ImageView imageView02 = (ImageView) findViewById(R.id.imageView_02);
        thread_02 = new CameraFeedThread(this, imageView02, imageSet_02, 1000);

        Button cameraButton_01 = (Button) findViewById(R.id.button_01);
        cameraButton_01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!thread_01.isAlive()) {
                    thread_01.startFeed();
                } else {
                    thread_01.stopFeed();
                }

            }
        });

        Button cameraButton_02 = (Button) findViewById(R.id.button_02);
        cameraButton_02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!thread_02.isAlive()) {
                    thread_02.startFeed();
                } else {
                    thread_02.stopFeed();
                }

            }
        });
    }

    @Override
    protected void onPause() {

        if (thread_01.isAlive()) {
            thread_01.pause();
        }

        if (thread_02.isAlive()) {
            thread_02.pause();
        }

        super.onPause();
    }

    @Override
    protected void onResume() {

        if (thread_01.didPause) {
            thread_01.startFeed();
        }

        if (thread_02.didPause) {
            thread_02.startFeed();
        }

        super.onResume();
    }

}
